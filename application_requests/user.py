from argon2 import PasswordHasher
from argon2.exceptions import VerificationError, HashingError
from database.db_setup import session
from database.application_models import User, UserRiotCredentials
from utils.errors import InvalidInput, InvalidToken
from sqlalchemy import func
import utils.input_validations as validations
from itsdangerous import URLSafeTimedSerializer, SignatureExpired, BadSignature
import app_config

__PATTERN_EMAIL = r'[\w.-]+@\w+.\w+'
__PASSWORD_LENGTH = (6, 21)
__USERNAME_LENGTH = (2, 17)

ph = PasswordHasher()


def create_user(username, password, email):
    try:
        check_username(username)
        check_email(email)
        check_password(password)
    except InvalidInput as err:
        raise err
    else:
        encrypted_hash = ph.hash(password)
        user = User(
            username=username,
            password=encrypted_hash,
            email=email
        )

        session.add(user)
        session.commit()
        return user


def add_user_riot_credentials(summoner, user_id):
    riot_credentials = UserRiotCredentials(
        riot_id=summoner['id'],
        riot_account_id=summoner['accountId'],
        summoner_name=summoner['name'],
        user_id=user_id
    )
    session.merge(riot_credentials)
    session.commit()


def authenticate(identifier, password):
    if validations.is_valid_email(identifier):
        user = session.query(User).filter(func.lower(User.email) == func.lower(identifier), User.deleted_at is not None).one()
    else:
        user = session.query(User).filter(func.lower(User.username) == func.lower(identifier), User.deleted_at is not None).one()

    try:
        ph.verify(user.password, password)
    except VerificationError:
        raise InvalidInput(401, "password doesn't match")
    else:
        return user


def generate_token(email):
    serializer = URLSafeTimedSerializer(app_config.SECRET_KEY)
    return serializer.dumps(email, salt=app_config.SALT)


def confirm_email_token(token, expiration=86400):  # 86400 = 1 DAY IN SECONDS
    serializer = URLSafeTimedSerializer(app_config.SECRET_KEY)
    try:
        email = serializer.loads(
            token,
            salt=app_config.SALT,
            max_age=expiration
        )
    except SignatureExpired:
        raise InvalidToken(400, "This link has expired")
    else:
        user = session.query(User).filter_by(email=email).one()
        user.validated = True
        session.commit()

    return email


def reset_password_token(token, expiration=86400):  # 86400 = 1 DAY IN SECONDS
    serializer = URLSafeTimedSerializer(app_config.SECRET_KEY)
    try:
        email = serializer.loads(
            token,
            salt=app_config.SALT,
            max_age=expiration
        )
    except SignatureExpired:
        raise InvalidToken(400, "This link has expired")
    else:
        return email


def check_username(username):
    if validations.check_length(username, *__USERNAME_LENGTH):
        if validations.is_valid_username(username):
            if user_exists(username):
                raise InvalidInput(409, 'username {username} already in use'.format(username=username))
        else:
            raise InvalidInput(409, 'username must contains only letters and `-` or `_` characters')
    else:
        raise InvalidInput(400, '{username} must have between {min} and {max} characters'.format(
            username=username, min=__USERNAME_LENGTH[0], max=__USERNAME_LENGTH[1]))


def check_email(email):
    if validations.is_valid_email(email):
        if email_exists(email):
            raise InvalidInput(409, 'email {email} already in use'.format(email=email))
    else:
        raise InvalidInput(400, '{email} is not a valid email'.format(email=email))


def check_password(password):
    if not validations.has_digit(password):
        raise InvalidInput(400, 'Password must have at least a digit')
    if not validations.has_lowercase(password):
        raise InvalidInput(400, 'Password must have at least a lowercase letter')
    if not validations.has_uppercase(password):
        raise InvalidInput(400, 'Password must have at least a uppercase letter')
    if not validations.has_specialchar(password):
        raise InvalidInput(400, 'Password must have at least a special char')
    if not validations.check_length(password, *__PASSWORD_LENGTH):
        raise InvalidInput(400, 'Password must have between {} and {} characters'.format(*__PASSWORD_LENGTH))


def user_exists(username):
    return session.query(User).filter(func.lower(User.username) == func.lower(username)).one_or_none()


def email_exists(email):
    return session.query(User).filter_by(email=email).one_or_none()


def update_password(identifier, password):
    if validations.is_valid_email(identifier):
        user = session.query(User).filter(func.lower(User.email) == func.lower(identifier), User.deleted_at is not None).one()
    else:
        user = session.query(User).filter(func.lower(User.username) == func.lower(identifier), User.deleted_at is not None).one()

    try:
        check_password(password)
        user.password = ph.hash(password)
    except InvalidInput as err:
        raise err
    else:
        session.commit()
        return True


if __name__ == '__main__':
    print(update_password('espinomlg@gmail.com', 'Ma.011235813'))
