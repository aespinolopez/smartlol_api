

function onSubmit() {
    event.preventDefault();
    if(validatePassword() && validateConfirmPassword()) {
        document.getElementById('reset-password-form').submit();
    }
}


function validatePassword() {
    var passwordInput = document.getElementById('new-password');
    var error = document.getElementById('error');
    if(!error) {
        error = createP();
        error.id = 'error';
    }

    if(!checkHasDigit(passwordInput.value)) {
        error.innerText = 'Password must contain a digit';
        passwordInput.parentElement.appendChild(error);
        return false;
    }
    if(!checkHasLowerCase(passwordInput.value)) {
        error.innerText = 'Password must contain a lowercase letter';
        passwordInput.parentElement.appendChild(error);
        return false;
    }
    if(!checkHasUpperCase(passwordInput.value)) {
        error.innerText = 'Password must contain an uppercase letter';
        passwordInput.parentElement.appendChild(error);
        return false;
    }
    if(!checkHasSpecialChars(passwordInput.value)) {
        error.innerText = 'Password must contain a special char';
        passwordInput.parentElement.appendChild(error);
        return false;
    }
    if(!checkLength(passwordInput.value, 6, 21)) {
        error.innerText = 'Password length must be at least 6 and 21 as maximum';
        passwordInput.parentElement.appendChild(error);
        return false;
    }

    error.remove();

    return true;
}

function validateConfirmPassword() {
    var passwordInput = document.getElementById('new-password');
    var confirmPasswordInput = document.getElementById('confirm-new-password');
    var error = document.getElementById('confirm-error');
    if(!error){
        error = createP();
        error.id = 'confirm-error';
    }

    if(passwordInput.value != confirmPasswordInput.value) {
        error.innerText = 'Passwords must be equals';
        confirmPasswordInput.parentElement.appendChild(error);
        return false;
    }else{
        if(error){
            error.remove();
        }
    }

    return true;
}


function checkHasDigit(string) {
    var has_digit_regex = /\w*\d+\w*/;
    return has_digit_regex.exec(string)
}

function checkHasLowerCase(string) {
    var has_lower_regex = /\w*[a-z]+\w*/;
    return has_lower_regex.exec(string);
}

function checkHasUpperCase(string) {
    var has_upper_regex = /\w*[A-Z]+\w/;
    return has_upper_regex.exec(string);
}

function checkHasSpecialChars(string) {
    var has_special_char_regex = /\w*[.@_\s#$=/*-]+\w*/;
    return has_special_char_regex.exec(string);
}

function checkLength(string, min, max) {
    return string.length > min && string.length < max;
}

function createP() {
    var p = document.createElement("p");
    p.style.color = 'red';

    return p;
}