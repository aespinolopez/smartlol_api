import app_config
import riot_requests.match as match
import riot_requests.summoner as summoner
import application_requests.user as users
from utils.errors import InvalidInput, InvalidToken
from utils.decorators import async
from flask import Flask, jsonify, abort, request, url_for, render_template, redirect
from flask_mail import Mail
from requests import HTTPError
import sqlalchemy.orm.exc as sqlexceptions
from flask_mail import Message
from itsdangerous import URLSafeTimedSerializer, SignatureExpired

from database.db_setup import session
from riot_requests import champion

import json


app = Flask(__name__)

app.config.update(dict(
    MAIL_SERVER=app_config.MAIL_SERVER,
    MAIL_PORT=app_config.MAIL_PORT,
    MAIL_USE_TLS=app_config.MAIL_USE_TLS,
    MAIL_USE_SSL=app_config.MAIL_USE_SSL,
    MAIL_USERNAME=app_config.MAIL_USERNAME,
    MAIL_PASSWORD=app_config.MAIL_PASSWORD
))

mail = Mail(app)


@app.route('/smartLoL/summoner/<summoner_name>/credentials', methods=["POST"])
def get_summoner_riot_credentials(summoner_name):
    try:
        summoner_request = summoner.SummonerRequest(request.args['platform'])
        summoner_data = summoner_request.get_summoner_info(summoner_name)
    except HTTPError as err:
        print(err.args[0].status_code)
        return abort(err.args[0].status_code)
    else:
        users.add_user_riot_credentials(summoner_data, request.form['user_id'])
        return jsonify(summoner_data)


@app.route('/smartLoL/summoner/<summoner_name>')
def get_summoner_info(summoner_name):
    try:
        summoner_request = summoner.SummonerRequest(request.args['platform'], request.args['language'])
        summoner_data = summoner_request.get_summoner_info(summoner_name)
        summoner_data['leagues'] = summoner_request.get_summoner_league(summoner_data['id'])
        summoner_data['top_champions'] = summoner_request.get_summoner_top_champions(summoner_data['id'])[0]
    except HTTPError as err:
        print(err.args[0].status_code)
        return abort(err.args[0].status_code)
    else:
        return jsonify(summoner_data)


@app.route('/smartLoL/recentgames/<account_id>')
def get_recent_games(account_id):
    try:
        match_request = match.MatchRequest(request.args['platform'], request.args['language'])
        recent_games = match_request.get_summoner_recent_matches(account_id)
    except HTTPError as err:
        return abort(err.args[0].status_code)
    else:
        return jsonify(recent_games)


@app.route('/smartLoL/recentrankeds/<account_id>')
def get_recent_rankeds(account_id):
    try:
        match_request = match.MatchRequest(request.args['platform'], request.args['language'])
        recent_rankeds = match_request.get_summoner_recent_rankeds(account_id)
    except HTTPError as err:
        return abort(err.args[0].status_code)
    else:
        return jsonify(recent_rankeds)


@app.route('/smartLoL/currentgame/<summoner_id>')
def get_current_game(summoner_id):
    # test = json.load(open('current-game-data-example.json')) # todo for testing purposes only
    # return jsonify(test)
    try:
        match_request = match.MatchRequest(request.args['platform'], request.args['language'])
        current_game = match_request.get_current_game_info(summoner_id)
    except HTTPError as err:
        return abort(err.args[0].status_code)
    else:
        return jsonify(current_game)


@app.route('/smartLoL/champions/')
def get_champions_list():
    champion_request = champion.ChampionRequest()
    return jsonify(champion_request.get_champions_list())


@app.route('/smartLoL/champion/<champion_id>')
def get_champion_info(champion_id):
    champion_request = champion.ChampionRequest(request.args['language'])
    return jsonify(**champion_request.get_champion_info(champion_id))


@app.route('/smartLoL/users', methods=['POST'])
def create_user():
    try:
        user = users.create_user(request.form['username'], request.form['password'], request.form['email'])
    except InvalidInput as err:
        abort(err.code, err.message)
    else:
        token = users.generate_token(user.email)
        url = url_for('confirm_email', token=token, _external=True)
        html = render_template('validate_email.html', confirm_url=url)
        send_app_email('confirm your account', app_config.ADMINS[0], [user.email], None, html)
        return jsonify({
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'validated': user.validated
        })


@app.route('/smartLoL/users/confirm-email/<token>')
def confirm_email(token):
    try:
        email = users.confirm_email_token(token)
    except InvalidToken as err:
        abort(err.code, err.message)
    else:
        return render_template('200.html', message='Email has been confirm')


@app.route('/smartLoL/users/confirm-email/resend', methods=["POST"])
def resend_confirmation_email():
    email = request.form['email']
    token = users.generate_token(email)
    url = url_for('confirm_email', token=token, _external=True)
    html = render_template('validate_email.html', confirm_url=url)
    send_app_email('confirm your account', app_config.ADMINS[0], [email], None, html)
    return '', 204


@app.route('/smartLoL/users/<username>')
def check_username_exists(username):
    try:
        users.check_username(username)
    except InvalidInput as err:
        abort(err.code, err.message)
    else:
        return "available", 204


@app.route('/smartLoL/users/login', methods=["POST"])
def authenticate():
    try:
        user = users.authenticate(request.form['identifier'], request.form['password'])
    except sqlexceptions.NoResultFound:
        abort(401, "username or email not found")
    except sqlexceptions.MultipleResultsFound:
        abort(401, "Severe error, check authentication logic. Username or email are duplicated")
    except InvalidInput as err:
        abort(err.code, err.message)
    else:
        return jsonify({
            'id': user.id,
            'username': user.username,
            'email': user.email,
            'validated': user.validated
        })


@app.route('/smartLoL/users/reset-password', methods=["POST"])
def send_reset_password_url():
    email = request.form['email']
    success = users.email_exists(email)
    if not success:
        abort(409, 'Email {} has not been registered'.format(email))
    token = users.generate_token(email)
    url = url_for('reset_password', token=token, _external=True)
    html = render_template('recover-password-email.html', recover_password_url=url)
    send_app_email('reset your password', app_config.ADMINS[0], [email], None, html)
    return '', 204


@app.route('/smartLoL/users/reset-password/<token>', methods=["GET"])
def reset_password(token):
    serializer = URLSafeTimedSerializer(app_config.SECRET_KEY)
    try:
        email = serializer.loads(
            token,
            salt=app_config.SALT,
            max_age=86400
        )
    except SignatureExpired:
        abort(400, "this token has expired")
    else:
        return render_template('recover-password.html', email=email)


@app.route('/smartLoL/users/update-password', methods=["POST"])
def update_password():
    try:
        users.update_password(request.form['identifier'], request.form['password'])
    except InvalidInput as err:
        abort(err.code, err.message)
    else:
        return render_template('200.html', message='Password has been updated')


@app.route('/')
@app.route('/index')
def smartlol_preview_web():
    return render_template('web-preview.html')


@app.teardown_appcontext
def shutdown_session(exception=None):
    session.close()


@async
def send_async_email(flask_app, message):
    with flask_app.app_context():
        mail.send(message)


def send_app_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    send_async_email(app, msg)


if __name__ == '__main__':
    app.secret_key = app_config.SECRET_KEY
    app.debug = True
    app.run(host='0.0.0.0', port=5001)

