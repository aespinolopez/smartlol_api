from sqlalchemy import create_engine, text
from database.db_config import Base, TESTS_DB
from sqlalchemy.orm import sessionmaker, scoped_session
from app_config import LOCALES


DB_URI = "mysql+mysqldb://{user}:{password}@{host}:{port}/{dbname}?charset=utf8"
engine = create_engine(DB_URI.format(**TESTS_DB))


session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))


def init_db():
    import database.staticdata_models
    import database.application_models
    Base.metadata.create_all(engine)
    insert_locales()


def insert_locales():
    for id_locale, locale in LOCALES.items():
        sql = text('INSERT INTO Languages VALUES ({id}, \'{locale}\')'.format(id=id_locale, locale=locale))
        engine.execute(sql)


if __name__ == '__main__':
    init_db()
