from sqlalchemy.orm import relationship
from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Text, Boolean
from sqlalchemy.dialects.mysql import SET
from database.db_config import Base
import datetime


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    username = Column(String(50), nullable=False, unique=True)
    password = Column(String(150), nullable=False)
    email = Column(String(150), nullable=False, unique=True)
    validated = Column(Boolean, default=False)
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
    deleted_at = Column(DateTime, nullable=True)

    riot_accounts = relationship("UserRiotCredentials", back_populates="user")
    # tips = relationship("CommunityTips", backref="users")
    # matchup_tips = relationship("MatchUpCommunityTips", backref="users")


class UserRiotCredentials(Base):
    __tablename__ = 'user_riot_credentials'

    riot_id = Column(Integer, primary_key=True, autoincrement=False)
    riot_account_id = Column(Integer, nullable=False)
    summoner_name = Column(String(50))
    user_id = Column(Integer, ForeignKey("users.id"))
    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow)
    deleted_at = Column(DateTime, nullable=True)

    user = relationship("User", back_populates="riot_accounts")


# class Youtubers(Base):
#     __tablename__ = 'youtubers'
#
#     id = Column(String(50), primary_key=True)
#     name = Column(String(50), nullable=False)
#     description = Column(Text, nullable=False)
#     country = Column(String(10), nullable=False)
#     lanes = Column(SET('top', 'mid', 'jungle', 'adc', 'support'), nullable=False)
#     user_id = Column(Integer, ForeignKey("users.id"), nullable=False)
#     created_at = Column(DateTime, default=datetime.datetime.utcnow)
#     updated_at = Column(DateTime, default=datetime.datetime.utcnow)
#     deleted_at = Column(DateTime, nullable=True)
#
#     champions = relationship("YoutubersPlayChampions")
#     # todo relationship between this entity and user?
#
#
# class YoutubersPlayChampions(Base):
#     __tablename__ = 'youtubers_play_champions'
#
#     id_youtuber = Column(String(50), ForeignKey("youtubers.id"), primary_key=True)
# #     id_champion = Column(Integer, ForeignKey("Champions.id"), primary_key=True)
# #     is_otp = Column(Boolean, default=False)
# #
# #     champion = relationship("Champions", uselist=False)
#
#
# class CommunityTips(Base):
#     __tablename__ = 'community_tips'
#
#     id = Column(Integer, primary_key=True, autoincrement=True)
#     id_user = Column(Integer, ForeignKey("users.id"), nullable=False)
#     id_champ = Column(Integer, ForeignKey("Champions.id"), nullable=False)
#     mode = Column(Boolean, nullable=False)
#     tip = Column(Text, nullable=False)
#
#     champion = relationship("Champions", uselist=False)
#
#
# class MatchUpCommunityTips(Base):
#     __tablename__='matchup_community_tips'
#     id = Column(Integer, primary_key=True, autoincrement=True)
#     id_user = Column(Integer, ForeignKey("users.id"), nullable=False)
#     id_source_champ = Column(Integer, ForeignKey("Champions.id"), nullable=False)
#     id_target_champ = Column(Integer, ForeignKey("Champions.id"), nullable=False)
#     tip = Column(Text, nullable=False)
