class Error(Exception):
    """Base class for exceptions in this module"""
    pass


class InvalidInput(Error):
    """
    Exceptions raised for invalid input such as incorrect email or existing unique field
    in database or insecure password

    Attributes:
        code -- status code for the http response
        message -- explanation of the error
    """
    def __init__(self, code, message):
        self.code = code
        self.message = message


class InvalidToken(Error):
    """
    Exceptions raised for invalid token such as secret error or expired token

    Attributes:
        code -- status code for the http response
        message -- explanation of the error
    """
    def __init__(self, code, message):
        self.code = code
        self.message = message
