import json
from app_config import LOCALES
from utils.riot_api_connection import get_response
# from database.db_setup import session todo uncomment
from database.testdb_setup import session
from database.staticdata_models import StaticDataVersions, Perks, PerkTrees, PerksTranslations, PerkTreesTranslations

URL = "http://ddragon.leagueoflegends.com/cdn/8.4.1/data/en_US/runesReforged.json"
# https://raw.communitydragon.org/8.4/plugins/rcp-be-lol-game-data/global/default/v1/perks.json alternative url

def write_files():
    """
     writes the json response from the api into several files
    :return: None
    """
    #    for id_locale, locale in LOCALES.items():
    data = get_response(URL)
    with open("perks-{locale}.json".format(locale='en_US'), "w") as outfile:
        json.dump(data, outfile)


def main():
    """
    for each locale json file reads the file parsing it to json and insert the data into the database
    :return: None
    """
    first_time = True
    #    for id_locale, locale in LOCALES.items():
    with open("perks-{locale}.json".format(locale='en_US'), "r") as inputfile:
        response = json.load(inputfile)

    try:
        perks_version = session.query(StaticDataVersions).filter_by(code="{} version".format(response['type'])).one()
    except:
        configdata = {
            "code": "perks version",
            "value": '8.4.1'
        }
        print('Añadiendo versión perks')
        perks_version = StaticDataVersions(**configdata)
        session.merge(perks_version)

    perks_data = response

    for perk_tree in perks_data:
        print(perk_tree['id'], perk_tree['key'], perk_tree['name'])
        if first_time:
            data = {
                "id": perk_tree['id'],
                "tree_key": perk_tree['key'],
                "image": perk_tree['icon']
            }
            exists = session.query(PerkTrees).filter_by(tree_key=perk_tree['key']).one_or_none()
            if exists:
                data['id'] = exists.id
            newPerkTree = PerkTrees(**data)
            session.merge(newPerkTree)

        translation = {
            "name": perk_tree['name'],
            "id_language": 1,
            "id_tree": perk_tree['id']
        }
        newTranslation = PerkTreesTranslations(**translation)
        session.merge(newTranslation)
        slot_index = 1
        for slot in perk_tree['slots']:
            for perk in slot['runes']:
                if first_time:
                    perk_data = {
                        "id": perk['id'],
                        "perk_key": perk['key'],
                        "slot": slot_index,
                        "image": perk['icon']
                    }
                    exists = session.query(PerkTrees).filter_by(tree_key=perk_tree['key']).one_or_none()
                    if exists:
                        perk_data['id'] = exists.id
                    newPerk = Perks(**perk_data)
                    newPerk.id_tree = newPerkTree.id
                    session.merge(newPerk)
                translation = {
                    'id_perk': perk['id'],
                    'id_language': 1,
                    'name': perk['name'],
                    'description': perk['shortDesc']
                }
                newPerkTranslation = PerksTranslations(**translation)
                session.merge(newPerkTranslation)
            slot_index += 1

    first_time = False
    print("todos los datos insertados con éxito para {}".format(1))
    session.commit()


def get_clean_description(description):
    pattern = r'<\w*>'


if __name__ == '__main__':
    write_files()
    main()
