from threading import Thread
from flask_mail import Message
from project import mail, app


def async(func):
    def wrapper(*args, **kwargs):
        thr = Thread(target=func, args=args, kwargs=kwargs)
        thr.start()
    return wrapper()


@async
def send_async_email(flask_app, message):
    with flask_app.app_context():
        mail.send(message)


def send_app_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    send_async_email(app, msg)
