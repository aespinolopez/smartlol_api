from utils.riot_api_connection import get_response
from database.staticdata_models import StaticDataVersions, SummonerSpells, SummonerSpellsTranslations
from database.db_setup import session
from app_config import LOCALES

import json

URL = "https://euw1.api.riotgames.com/lol/static-data/v3/summoner-spells?locale={}&dataById=false&tags=all"


def write_files():
    """
    writes the json response from the api into several files
    :return: None
    """
    for id_locale, locale in LOCALES.items():
        response = get_response(URL.format(locale))
        with open("summspells{}.json".format(locale), "w") as outfile:
            json.dump(response, outfile)


def main():
    """
    for each locale json file reads the file parsing it to json and insert the data into the database
    :return: None
    """
    first_time = True
    for id_locale, locale in LOCALES.items():
        with open("summspells{}.json".format(locale)) as inputfile:
            response = json.load(inputfile)

        try:
            summspell_version = session.query(StaticDataVersions).filter_by(code="{} version".format(response['type'])).one()
        except:
            configdata = {
                "code": "{} version".format(response['type']),
                "value": response['version']
            }
            print("añadiendo versión summoner spells")
            summspell_version = StaticDataVersions(**configdata)
            session.merge(summspell_version)
            session.commit()

        if summspell_version.value != response['version']:
            summspell_version.version = response['version']
            session.merge(summspell_version)

        summspells_data = response['data']

        for spell in summspells_data.values():
            if first_time:
                data = {
                    "id": spell['id'],
                    "summspell_key": spell['key'],
                    "image": spell['image']['full']
                }
                exists = session.query(SummonerSpells).filter_by(summspell_key=spell['key']).one_or_none()
                if exists:
                    data['id'] = exists.id
                newSummSpell = SummonerSpells(**data)
                session.merge(newSummSpell)

            translation = {
                "id_spell": spell['id'],
                "id_language": id_locale,
                "name": spell['name'],
                "description": spell['sanitizedDescription']
            }
            newTranslation = SummonerSpellsTranslations(**translation)
            session.merge(newTranslation)

        first_time = False
        print("todos los datos insertados con éxito para {}".format(locale))

    session.commit()


if __name__ == '__main__':
    write_files()
    main()
