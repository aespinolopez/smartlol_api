import re

__PATTERN_EMAIL = r'[\w.-]+@\w+.\w+'
__PATTERN_SPECIAL_CHARS = r'\w*[-.@_\s#$=/*]+\w*'
__PATTERN_USERNAME_ALLOW_CHARS = r'\w*[\w_-]+\w*'


def has_uppercase(someinput):
    pattern = r'\w*[A-Z]+\w*'
    return re.search(pattern, someinput)


def has_lowercase(someinput):
    pattern = r'\w*[a-z]+\w*'
    return re.search(pattern, someinput)


def has_digit(someinput):
    pattern = r'\w*\d+\w*'
    return re.search(pattern, someinput)


def has_specialchar(someinput):
    return re.search(__PATTERN_SPECIAL_CHARS, someinput)


def is_valid_username(someinput):
    return re.fullmatch(__PATTERN_USERNAME_ALLOW_CHARS, someinput)


def check_length(someinput, start, end):
    return len(someinput) in range(start, end)


def is_valid_email(email):
    match = re.fullmatch(__PATTERN_EMAIL, email)
    return match


def is_digit(char):
    pattern = r'\d'
    return re.fullmatch(pattern, char)

